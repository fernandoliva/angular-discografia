import { DiskInterface } from './../../models/disk.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-discs',
  templateUrl: './discs.component.html',
  styleUrls: ['./discs.component.scss']
})
export class DiscsComponent implements OnInit {

  discs: DiskInterface[] = []

  disk:DiskInterface = {
    name: '',
    year: 0,
    author:'',
    genre:'',
    img:'',
    tracksNumber:0
  }

  albumArray: DiskInterface[] = [
    {
      name: 'Ride the Lightning',
      year: 1984,
      author: 'Metallica',
      genre: 'Metal',
      img: 'https://images-na.ssl-images-amazon.com/images/I/71jyH5WtmFL._SL1425_.jpg',
      tracksNumber:8
    },
    {
      name: 'Vulgar Display of Power',
      year: 1992,
      author: 'Pantera',
      genre: 'Groove Metal',
      img: 'https://mariskalrock.com/wp-content/uploads/2020/03/vulgar-display-of-power-pantera.jpg',
      tracksNumber: 11
    },
    {
      name: 'Speed Metal Symphony',
      year: 1987,
      author: 'Cacophony',
      genre: 'Speed Metal',
      img: 'https://www.guitarcalavera.com/wp-content/uploads/2014/12/portada-Cacophony-Speed-Metal-Symphony.jpg',
      tracksNumber: 7
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  insertDisk(){
    // console.log(this.disk);
    this.discs.push(this.disk);
    console.log(this.discs);
  }

}
