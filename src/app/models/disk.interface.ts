export interface DiskInterface{
  name:string,
  year:number,
  author:string,
  genre:string,
  img:string,
  tracksNumber:number
}
